# frozen_string_literal: true

module ReleaseTools
  module Security
    class SyncRemotesService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Services::SyncBranchesHelper

      # Syncs master branch across all remotes
      def initialize
        @branches = %w[master]

        @projects = [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::GitlabCe,
          ReleaseTools::Project::OmnibusGitlab,
          ReleaseTools::Project::Gitaly
        ]
      end

      def execute
        @projects.each do |project|
          logger.info('Syncing branches', project: project, branches: @branches)

          sync_branches(project, *@branches)
        end
      end
    end
  end
end
