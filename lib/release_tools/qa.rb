# frozen_string_literal: true

module ReleaseTools
  module Qa
    TEAM_LABELS = [
      'Community contribution',
      'group::access',
      'group::compliance',
      'group::import',
      'group::analytics',
      'group::spaces',
      'group::project management',
      'group::product planning',
      'group::source code',
      'group::knowledge',
      'group::static site editor',
      'group::editor',
      'group::gitaly',
      'group::gitter',
      'group::ecosystem',
      'group::continuous integration',
      'group::pipeline authoring',
      'group::runner',
      'group::testing',
      'group::package',
      'group::progressive delivery',
      'group::release management',
      'group::configure',
      'group::apm',
      'group::health',
      'group::static analysis',
      'group::dynamic analysis',
      'group::composition analysis',
      'group::fuzz testing',
      'group::vulnerability research',
      'group::container security',
      'group::threat insights',
      'group::acquisition',
      'group::conversion',
      'group::expansion',
      'group::retention',
      'group::fulfillment',
      'group::telemetry',
      'group::distribution',
      'group::geo',
      'group::memory',
      'group::global search',
      'group::database',
      'group::provision',
      'group::not_owned'
    ].freeze

    PROJECTS =
      if Feature.enabled?(:qa_multi_project)
        [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::Gitaly
        ].freeze
      else
        [
          ReleaseTools::Project::GitlabEe
        ].freeze
      end
  end
end
