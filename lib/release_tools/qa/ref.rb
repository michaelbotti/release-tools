# frozen_string_literal: true

module ReleaseTools
  module Qa
    class Ref
      TAG_REGEX = /(?<prefix>\w?)(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)(-rc?(?<rc>\d+))?/.freeze
      STABLE_BRANCH_REGEX = /^(?<major>\d+)-(?<minor>\d+)-(?<stable>stable)$/.freeze

      AUTO_DEPLOY_TAG_REGEX = /
        \A
        (?<prefix>\w?)
        (?<major>\d+)
        \.
        (?<minor>\d+)
        \.
        (?<patch>\d+)
        -
        (?<commit>\h{11,})
        \.
        (?<omnibus_commit>\h{11,})
        \z
      /x.freeze

      def initialize(ref)
        @ref = ref
      end

      def ref
        matches = @ref.match(AUTO_DEPLOY_TAG_REGEX)

        if matches && matches[:commit]
          matches[:commit]
        else
          @ref
        end
      end

      def for_project(project)
        return for_project_or_component(project) if Feature.enabled?(:qa_multi_project)

        if project == ReleaseTools::Project::GitlabEe && include_suffix?
          "#{ref}-ee"
        else
          ref
        end
      end

      # Experimental multi-project QA issues
      #
      # TODO (rspeicher): After removal, this method body can replace `for_project`.
      #
      # See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1304
      def for_project_or_component(project)
        given_ref = ref

        if project == ReleaseTools::Project::GitlabEe && include_suffix?
          given_ref.concat('-ee')
        end

        if project.respond_to?(:version_file)
          # When given a component project, we need its version in GitLab EE
          Retriable.retriable(on: Gitlab::Error::ResponseError) do
            GitlabClient.file_contents(
              Project::GitlabEe,
              project.version_file,
              given_ref
            ).chomp
          end
        else
          given_ref
        end
      end

      private

      def include_suffix?
        tag? || stable_branch?
      end

      def tag?
        ref.match?(TAG_REGEX) && !ref.match?(AUTO_DEPLOY_TAG_REGEX)
      end

      def stable_branch?
        ref.match?(STABLE_BRANCH_REGEX)
      end
    end
  end
end
