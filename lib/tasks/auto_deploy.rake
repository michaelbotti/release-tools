# frozen_string_literal: true

namespace :auto_deploy do
  task :check_enabled do
    if ReleaseTools::Feature.disabled?(:auto_deploy)
      ReleaseTools.logger.warn("The `auto_deploy` feature flag is currently disabled.")
      exit
    end
  end

  desc "Prepare for auto-deploy by creating branches from the latest green commit on gitlab and omnibus-gitlab"
  task prepare: :check_enabled do
    ReleaseTools::Tasks::AutoDeploy::Prepare.new.execute
  end

  desc 'Pick commits into the auto deploy branches'
  task pick: :check_enabled do
    pick = lambda do |project, branch_name|
      ReleaseTools.logger.info(
        'Picking into auto-deploy branch',
        project: project.auto_deploy_path,
        target: branch_name
      )

      ReleaseTools::CherryPick::AutoDeployService
        .new(project, branch_name)
        .execute
    end

    branch_name = ReleaseTools::AutoDeployBranch.current_name

    pick[ReleaseTools::Project::GitlabEe, branch_name]
    pick[ReleaseTools::Project::OmnibusGitlab, branch_name]
  end

  desc "Tag the auto-deploy branches from the latest passing builds"
  task tag: :check_enabled do
    branch = ReleaseTools::AutoDeployBranch.current
    version = "#{branch.version.to_minor}.#{branch.tag_timestamp}"
    metadata = ReleaseTools::ReleaseMetadata.new

    commit = ReleaseTools::PassingBuild
      .new(branch.to_s)
      .execute

    ReleaseTools::AutoDeploy::Builder::Omnibus
      .new(branch, commit.id, metadata)
      .execute

    ReleaseTools::AutoDeploy::Builder::CNGImage
      .new(branch, commit.id, metadata)
      .execute

    ReleaseTools::Deployments::SentryTracker.new.release(commit.id)

    unless dry_run?
      ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata)
    end
  end

  desc "Validate production pre-checks"
  task :check_production do
    ReleaseTools::Tasks::AutoDeploy::CheckProduction.new.execute
  end

  desc 'Cleans up old auto-deploy branches'
  task :cleanup do
    ReleaseTools::AutoDeploy::Cleanup.new.cleanup
  end
end
