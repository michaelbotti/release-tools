# GitLab Release Tools

This repository contains instructions and tools for releasing new versions of
GitLab Community Edition (CE) and Enterprise Edition (EE).

The goal is to provide clear instructions and procedures for our entire release
process, along with automated tools, to help anyone perform the role of [Release
Manager](doc/release-manager.md).

## Documentation

- [release/docs](https://gitlab.com/gitlab-org/release/docs/blob/master/README.md)
  documents the entire release process for developers, release managers, and the
  security and quality teams
- [Rake tasks](doc/rake-tasks.md) contained in this project
- [ChatOps triggers](doc/chatops.md) available from this project
- [Issue templates](./templates) used for creating the release task lists
- [CI variables](doc/variables.md) used by this project
- [Metrics](doc/metrics.md) contains information on developing metrics gathered
  by this project
- [Releases using the API](doc/api-releases.md) provides information about the
  code used for performing releases using the API

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
